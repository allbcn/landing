import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import './static/css/app.css';

import pinkBackground from './static/media/pinkBackground.png';
import moviles1 from './static/media/moviles1.png';
import moviles2 from './static/media/moviles2.png';
import moviles3 from './static/media/moviles3.png';
import bubble1 from './static/media/bubble1.png';
import bubble2 from "./static/media/bubble2.png";
import bubble3 from './static/media/bubble3.png';
import bubble4 from "./static/media/bubble4.png";
import bubble5 from "./static/media/bubble5.png";
import logoBlack from "./static/media/logoBlack.png";
import slogan from "./static/media/slogan.png";
import slogan2 from "./static/media/slogan2.png";
import logoApple from "./static/media/logoApple.svg";
import logoGoogle from "./static/media/logoGoogle.png";

class App extends Component {
    render() {
        return (
            <Router>
                <div  className="App">
                  <div className="tab"
                  >
                    <Link to="/">
                      <img className="logoBlack" src={logoBlack} />
                    </Link>
                    <Link to="/faqs">
                      <button className="tablinks">FAQs</button>
                    </Link>
                    <Link to="/">
                      <button className="tablinks">Sobre nosotros</button>
                    </Link>
                    <Link to="/">
                      <button className="tablinks">Inicio</button>
                    </Link>
                  </div>
                    <Route exact path="/" component={Home}/>
                    <Route path="/team" component={Team}/>
                    <Route path="/faqs" component={FAQs}/>
                </div>
            </Router>
        );
    }
}
const Home = () => (
  <div className="Wrapper">
    <div className="Image1" style ={ { backgroundImage: "url("+pinkBackground+")" } }>
        <img className="Slogan2" src={slogan2} />
        <div className="Paragraph1">
          <h1 className="Text1">VENDER Y COMPRAR NUNCA FUE TAN FÁCIL, CÓMODO Y SEGURO</h1>
          <h2 className="Text2">Descubrí una forma de llegar a más personas y encontrar lo que necesitás de una manera más rápida.</h2>
          <img className="LogoApple" src={logoApple} />
          <img className="LogoGoogle" src={logoGoogle} />
        </div>
        <img className="Moviles1" src={moviles1} />
    </div>
    <div className="Image2">
      <div className="column Text3">
          <h1>ENCONTRÁ LO QUE BUSCÁS, RAPIDÍSIMO</h1>
          <h2>No pierdas tiempo buscando y buscando. Con el buscador encontrás el producto que necesitás, al precio que podés pagar.</h2>
      </div>
      <img className=" column Moviles2" src={moviles2} />
    </div>
    <div className="Image3">
      <div className="Bubbles">
          <img className="Bubble Bubble1" src={bubble1} />
          <img className="Bubble Bubble2" src={bubble2} />
          <img className="Bubble Bubble3" src={bubble3} />
          <img className="Bubble Bubble4" src={bubble4} />
      </div>
      <div className="Paragraph2">
          <h1> CONVERSÁ DENTRO DE LA APLICACIÓN</h1>
          <h2>Sin esperar a que contesten tu comentario, sin necesidad de dar tu número.</h2>
      </div>
    </div>
    <div className="Image4" style ={ { backgroundImage: "url("+moviles3+")" } }>
        <img className="Bubble5" src={bubble5} />
        <div className="Text4">
            <h1>GESTIONÁ TODAS TUS COMPRAS</h1>
            <h2>Todo al alcance de tu mano. Guardá lo que más te guste y hacé todos los cambios que necesites sin complicaciones</h2>
        </div>
    </div>
    <div className="Image5">
      <div className="column2">
        <img className="Slogan" src={slogan} />
      </div>
      <div className="column2">
        <h4>All</h4>
        <p>¿Quienes somos?</p>
        <p>Empleo</p>
        <p>Equipo</p>
      </div>
      <div className="column2">
        <h4>Soporte</h4>
        <p> Preguntas frecuentes</p>
        <p>Reglas de convivencia </p>
        <p>Consejos de seguridad</p>
      </div>
      <div className="column2">
        <h4>Legal</h4>
        <p>Condiciones de uso</p>
        <p>Políticas de privacidad</p>
        <p>Cookies</p>
      </div>
    </div>
  </div>
);

const Team = () => (
  <div>
    <h1>Team</h1>
  </div>
);

const FAQs = () => (
  <div className="Wrapper">
    <div className="FAQs">
      <h4 className="titleFAQs">Preguntas frecuentes</h4>
      <h5>¿Cómo publico un producto?</h5>
      <h6>¡Súper fácil! Le tomás un par de fotografías, las seleccionas, estableces el precio, marcas la categoría y le das a publicar. ¡Ya está! Tu anuncio publicado.</h6>


      <h5>¿Cómo contacto con un vendedor?</h5>
      <h6>Selecciona el producto que le interese y haciendo click en el botón "Chat" podrá hablar directamente con el vendedor.</h6>

      <h5>¿La aplicación es gratuita?</h5>
      <h6>El servicio y la aplicación son totalmente gratuitos. Nada más debés descargarlas y ya.</h6>

      <h5>¿Para que verifican el perfil?</h5>
      <h6>Usamos la verificación para garantizar que los usuarios sean reales y así poder ofrecer una mejor experiencia.</h6>

      <h5>¿AllApp fija los precios?</h5>
      <h6>Los precios son fijados directamente por los usuarios. No tenemos ninguna relación con las negociaciones que se den dentro ni los precios que se establecen.</h6>

      <h5>¿Para qué necesitan acceder a mi ubicación?</h5>
      <h6>Accedemos a tu ubicación para poder mostrarte los productos que se encuentran más cerca de vos. De manera que puedas obtener lo que necesites lo más pronto posible.</h6>

      <h5>¿Quiénes pueden ver mi productos?</h5>
      <h6>Todos los usuarios de All tienen acceso a la información de los productos que ofertan.</h6>
    </div>
    <div className="Image5">
      <div className="column2">
        <img className="Slogan" src={slogan} />
      </div>
      <div className="column2">
        <h4>All</h4>
        <p>¿Quienes somos?</p>
        <p>Empleo</p>
        <p>Equipo</p>
      </div>
      <div className="column2">
        <h4>Soporte</h4>
        <p> Preguntas frecuentes</p>
        <p>Reglas de convivencia </p>
        <p>Consejos de seguridad</p>
      </div>
      <div className="column2">
        <h4>Legal</h4>
        <p>Condiciones de uso</p>
        <p>Políticas de privacidad</p>
        <p>Cookies</p>
      </div>
    </div>
  </div>

);
export default App;
